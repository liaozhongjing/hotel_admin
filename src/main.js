import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element'

//引入时间转化插件
//将2021-10-28T16:00:00.000Z 格式转化为 2020-09-30 02:02:02 
import dayjs from 'dayjs'

//引入element-ui样式表
import 'element-ui/lib/theme-chalk/index.css'
//引入Element 额外提供了一系列类名，用于在某些条件下隐藏元素。
import 'element-ui/lib/theme-chalk/display.css';



Vue.prototype.dayjs = dayjs

Vue.config.productionTip = false

//事件总线的使用
Vue.prototype.$bus = new Vue()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')



