import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: () => import('views/Login')
  },
  {
    path: '/home',
    component: () => import('views/home/Home'),
    children: [
      {
        path: '/',
        redirect: 'welcome'
      },
      {
        path: 'welcome',
        component: () => import('views/home/childrens/Welcome')
      },
      {
        path: 'user',
        component: () => import('views/home/childrens/User')
      },
      {
        path: 'staff',
        component: () => import('views/home/childrens/Staff')
      },
      {
        path: 'room',
        component: () => import('views/home/childrens/Room')
      },
      {
        path: 'room_params',
        component: () => import('views/home/childrens/Room_Params')
      },
      {
        path: 'room_pic',
        component: () => import('views/home/childrens/Room_pic')
      },
      {
        path: 'add_room',
        component: () => import('views/home/childrens/AddRoom')
      },
      {
        path: 'sub_order',
        component: () => import('views/home/childrens/Sub_order')
      },
      {
        path: 'enter_order',
        component: () => import('views/home/childrens/Enter_order')
      }
      ,
      {
        path: 'finish_order',
        component: () => import('views/home/childrens/Finish_order')
      }
      ,
      {
        path: 'comment',
        component: () => import('views/home/childrens/Comment')
      }
      ,
      {
        path: 'income',
        component: () => import('views/home/childrens/Income')
      },
      {
        path: 'room_type',
        component: () => import('views/home/childrens/Room_type')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//导航守卫  除了登录页面和注册页面其他页面需要token认证
router.beforeEach((to, from, next) => {
  if (to.path == '/login' || to.path == '/register')
    return next()
  const token = window.sessionStorage.getItem('token')
  //如果没有token转到登录页
  if (!token) return next('/login')
  //否则放行
  next()
})


export default router
