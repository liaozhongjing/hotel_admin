import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username: ''
  },
  mutations: {
    //修改username
    editUserName(state, username) {
      state.username = username
    }
  },
  actions: {
  },
  modules: {
  },
  getters: {
    getUsrname(state) {
      return state.username
    }
  }
})
