import axios from 'axios'

export function request(config) {
    axios.defaults.baseURL = 'http://localhost:3000'
    axios.timeout = 5000
    // axios.defaults.withCredentials = true
    //请求拦截器
    axios.interceptors.request.use(config => {
        //权限管理
        //每次发送请求携带token 将token保存在请求头中
        //Authorization这个属性时服务器接口文档自定义的
        config.headers.Authorization = window.sessionStorage.getItem('token');
        //console.log(config);
        return config
    })
    return axios(config)
}